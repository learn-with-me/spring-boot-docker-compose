package com.dhiren.springbootdockercompose.repository;

import com.dhiren.springbootdockercompose.entity.Guardian;
import com.dhiren.springbootdockercompose.entity.Student;
import com.dhiren.springbootdockercompose.repository.primary.StudentRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
//@DataJpaTest
class StudentRepositoryTest {

    @Autowired
    private StudentRepository repository;

    @BeforeAll
    static void setUp() {
        System.err.println("Started");
    }

    @AfterAll
    static void tearDown() {
        System.err.println("Completed");
    }

    @Test
    public void saveStudentWithGuardian() {
        repository.save(Student.builder()
                .emailId("rohit@bcci.com")
                .firstName("Rohit")
                .lastName("Sharma")
                .guardian(
                        Guardian.builder()
                                .name("vikash kumar")
                                .mobileNumber("232323232323")
                                .email("vikash@gmail.com")
                                .build())
                .build());
    }

    @Test
    public void getAllStudents() {
        System.out.println(repository.findAll());
    }

    @Test
    public void findAllStudentsByFirstName() {
        System.out.println(repository.findByFirstName("Dhiren"));
    }

    @Test
    public void findAllStudentsByFirstNameContaining() {
        System.out.println(repository.findByFirstNameContaining("Dhiren"));
    }

    @Test
    public void findAllStudentsLastNameNotNull() {
        System.out.println(repository.findByLastNameNotNull());
    }

    @Test
    public void findAllStudentsByGuardianName() {
        System.out.println(repository.findByGuardianName("Kumar"));
    }

    @Test
    public void findStudentByEmail() {
        System.out.println(repository.getStudentByEmailAddress("abc@xyz.com"));
    }

    @Test
    public void findStudentNameByEmail() {
        System.out.println(repository.getStudentFirstNameByEmailAddress("abc@xyz.com"));
    }

    @Test
    public void findStudentByEmailNative() {
        System.out.println(repository.getStudentByEmailAddressNative("abc@xyz.com"));
    }

    @Test
    public void findStudentByEmailNativeAndNamed() {
        System.out.println(repository.getStudentByEmailAddressNativeAndNamed("abc@xyz.com"));
    }

    @Test
    public void updateStudentByEmailNativeAndNamed() {
        System.out.println(repository.updateStudentNameByEmail("Mr.Dhiren","abc@xyz.com"));
    }
}