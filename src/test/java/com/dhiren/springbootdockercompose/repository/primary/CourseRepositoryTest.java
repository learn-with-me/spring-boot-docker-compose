package com.dhiren.springbootdockercompose.repository.primary;

import com.dhiren.springbootdockercompose.entity.Course;
import com.dhiren.springbootdockercompose.entity.Teacher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CourseRepositoryTest {

    @Autowired
    private CourseRepository courseRepository;

    @Test
    public void printCourses() {
        System.out.println(courseRepository.findAll());
    }

    @Test
    public void saveCourseWithTeacherDetails() {
        Teacher teacher = Teacher.builder().firstName("John").lastName("Doe").build();

        Course course = courseRepository.findCourseByCourseTitle("Spring Master Class");

        course.setTeacher(teacher);

        courseRepository.save(course);
    }

}