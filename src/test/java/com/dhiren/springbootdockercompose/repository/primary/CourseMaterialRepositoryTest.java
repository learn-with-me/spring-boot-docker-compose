package com.dhiren.springbootdockercompose.repository.primary;

import com.dhiren.springbootdockercompose.entity.Course;
import com.dhiren.springbootdockercompose.entity.CourseMaterial;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CourseMaterialRepositoryTest {

    @Autowired
    private CourseMaterialRepository repository;

    @Test
    public void saveCourseMaterial() {
        Course course = Course.builder().courseTitle("JPA Master Class").credit(10).build();
        CourseMaterial material = CourseMaterial.builder()
                .url("www.google.com/jpa-master").course(course).build();
        repository.save(material);
    }

    @Test
    public void saveCourseMaterial2() {
        Course course = Course.builder().courseTitle("Spring Master Class").credit(6).build();
        CourseMaterial material = CourseMaterial.builder()
                .url("www.google.com/spring-master").course(course).build();
        repository.save(material);
    }

    @Test
    public void fetchCourseMaterials() {
        System.out.println(repository.findAll());
    }

}