package com.dhiren.springbootdockercompose.repository.primary;

import com.dhiren.springbootdockercompose.entity.Course;
import com.dhiren.springbootdockercompose.entity.Teacher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TeacherRepositoryTest {

    @Autowired
    private TeacherRepository teacherRepository;


    // Relation Mapped by Course not Teached as @ManyToOne side owns the relationship
    @Test
    public void saveTeacher() {
        Teacher teacher = Teacher.builder().firstName("Ranga").lastName("Karannam")
                /*.courses(Arrays.asList(
                        Course.builder().courseTitle("Java Developer Master").credit(10).build(),
                        Course.builder().courseTitle("Spring Boot API Master").credit(15).build())
                )*/.build();
        teacherRepository.save(teacher);
    }

}