package com.dhiren.springbootdockercompose.service;

import com.dhiren.springbootdockercompose.model.Employer;
import com.dhiren.springbootdockercompose.model.Response;
import com.dhiren.springbootdockercompose.repository.secondary.EmployerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployerService {

    private EmployerRepository repository;

    @Autowired
    public EmployerService(EmployerRepository repository) {
        this.repository = repository;
    }

    public Employer save(Employer item) {
        return repository.save(item);
    }

    public List<Response> findAll() {
        return repository.findAll().stream().map(Response::new).collect(Collectors.toList());
    }
}
