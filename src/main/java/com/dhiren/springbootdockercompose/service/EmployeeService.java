package com.dhiren.springbootdockercompose.service;

import com.dhiren.springbootdockercompose.model.Employee;
import com.dhiren.springbootdockercompose.model.Response;
import com.dhiren.springbootdockercompose.repository.primary.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private EmployeeRepository repository;

    @Autowired
    public EmployeeService(EmployeeRepository repository) {
        this.repository = repository;
    }

    public Employee save(Employee item) {
        return repository.save(item);
    }

    public List<Response> findAll() {
        return repository.findAll().stream().map(Response::new).collect(Collectors.toList());
    }
}
