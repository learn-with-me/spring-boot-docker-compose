package com.dhiren.springbootdockercompose.model;


import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Data
@Entity
@Table
public class Department {
    @Id
    private String id;
    public String name;
    public int age;
}
