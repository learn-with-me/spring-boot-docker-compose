package com.dhiren.springbootdockercompose.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Configuration;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Configuration
public class Solutions {

    private String name;
    private String url;
    private String changeLog;
    private String databaseChangeLogLockTable;
    private String databaseChangeLogTable;
}
