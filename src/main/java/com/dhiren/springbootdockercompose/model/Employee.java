package com.dhiren.springbootdockercompose.model;


import liquibase.change.DatabaseChange;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Data
@Entity
@Table
public class Employee {
    @Id
    private String id;
    public String name;
    public int age;
}
