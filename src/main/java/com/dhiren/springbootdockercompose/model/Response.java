package com.dhiren.springbootdockercompose.model;

import lombok.Data;

import java.time.Instant;

@Data
public class Response extends Employee {
    private Instant returnedTime;

    public Response(Employee emp) {
        returnedTime = Instant.now();
        this.name = emp.name;
        this.age = emp.age;
    }

    public Response(Employer emp) {
        returnedTime = Instant.now();
        this.name = emp.name;
        this.age = emp.age;
    }
}
