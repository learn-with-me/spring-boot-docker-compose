package com.dhiren.springbootdockercompose.repository.primary;

import com.dhiren.springbootdockercompose.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by khawar on 2/13/19.
 */
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
