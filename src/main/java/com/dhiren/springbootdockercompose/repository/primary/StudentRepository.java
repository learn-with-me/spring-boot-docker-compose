package com.dhiren.springbootdockercompose.repository.primary;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.dhiren.springbootdockercompose.entity.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    List<Student> findByFirstName(String firstName);
    List<Student> findByFirstNameContaining(String name);
    List<Student> findByLastNameNotNull();
    List<Student> findByGuardianName(String name);

    @Query("select s from Student s where s.emailId=?1")
    Student getStudentByEmailAddress(String emailId);

    @Query("select s.firstName from Student s where s.emailId=?1")
    String getStudentFirstNameByEmailAddress(String emailId);

    @Query(value = "select * from TBL_STUDENT s where s.email_address=?1",
            nativeQuery = true)
    Student getStudentByEmailAddressNative(String emailId);

    @Query(
            value = "select * from TBL_STUDENT s where s.email_address=:emailId",
            nativeQuery = true)
    Student getStudentByEmailAddressNativeAndNamed(String emailId);

    @Query(
            nativeQuery = true,
            value = "update TBL_STUDENT set firstName=:newName where email_address=:emailId")
    @Modifying
    @Transactional
    int updateStudentNameByEmail(String newName, String emailId);

}
