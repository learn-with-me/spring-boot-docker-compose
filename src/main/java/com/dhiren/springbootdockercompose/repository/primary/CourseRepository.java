package com.dhiren.springbootdockercompose.repository.primary;

import com.dhiren.springbootdockercompose.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

    Course findCourseByCourseTitle(String title);
}
