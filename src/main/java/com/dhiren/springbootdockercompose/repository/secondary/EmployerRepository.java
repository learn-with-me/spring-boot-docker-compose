package com.dhiren.springbootdockercompose.repository.secondary;

import com.dhiren.springbootdockercompose.model.Employer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployerRepository extends JpaRepository<Employer, Long> {

}
