package com.dhiren.springbootdockercompose.controller;

import com.dhiren.springbootdockercompose.model.Response;
import com.dhiren.springbootdockercompose.service.EmployeeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api")
public class EmployeeController {

    private EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/employees")
    public ResponseEntity<List<Response>> findAll() {
        System.out.println("Request Received");
        return ResponseEntity.ok(employeeService.findAll());
    }

    @GetMapping("/test")
    @ResponseBody
    public Map<String, String> test() throws JsonProcessingException {
        System.out.println("Request Received");
        Map<String, String> map = new HashMap<>();
        map.put("key", "value");
        map.put("key1", "value1");
        MyMap<String,String> myMap = new MyMap<String,String>(map);
        return myMap;
    }
}

class MyMap<K,V> extends HashMap<K,V> {
    private MyMap<K, V> map;

    MyMap(Map<K, V> map) {
        this.map = (MyMap<K, V>) new HashMap<K,V>();
    }

    public MyMap<K, V> getMap() {
        return map;
    }
}