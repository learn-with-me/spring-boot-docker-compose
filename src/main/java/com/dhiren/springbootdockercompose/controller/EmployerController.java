package com.dhiren.springbootdockercompose.controller;

import com.dhiren.springbootdockercompose.model.Response;
import com.dhiren.springbootdockercompose.service.EmployerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployerController {

    private EmployerService employerService;

    @Autowired
    public EmployerController(EmployerService employerService) {
        this.employerService = employerService;
    }

    @GetMapping("/employers")
    public ResponseEntity<List<Response>> findAll() {
        System.out.println("Request Received");
        return ResponseEntity.ok(employerService.findAll());
    }

}
