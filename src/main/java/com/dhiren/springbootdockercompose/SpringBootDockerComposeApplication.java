package com.dhiren.springbootdockercompose;

import com.dhiren.springbootdockercompose.model.Employee;
import com.dhiren.springbootdockercompose.model.Employer;
import com.dhiren.springbootdockercompose.repository.primary.EmployeeRepository;
import com.dhiren.springbootdockercompose.repository.secondary.EmployerRepository;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.datatype.DataTypeFactory;
import liquibase.exception.DatabaseException;
import liquibase.integration.spring.SpringLiquibase;
import liquibase.sqlgenerator.core.CreateTableGenerator;
import liquibase.statement.core.CreateTableStatement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@SpringBootApplication
public class SpringBootDockerComposeApplication {

    @Autowired
    LiquibaseProperties liquibaseProperties;

    @Autowired
    SpringLiquibase primaryLiquibase;

    //Liquibase liquibase = new Liquibase();

    @Autowired
    public SpringBootDockerComposeApplication(EmployeeRepository employeeRepository, EmployerRepository employerRepository) {
        this.employeeRepository = employeeRepository;
        this.employerRepository = employerRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDockerComposeApplication.class, args);
    }

    private EmployeeRepository employeeRepository;
    private EmployerRepository employerRepository;

    @Bean
    public CommandLineRunner myCommandLineRunner(ApplicationContext ctx) throws DatabaseException {
        CreateTableGenerator generator = new CreateTableGenerator();
        generator.generateSql(
                getCreateTableStatement(getDataBase()),
                getDataBase(),
                null
                );

        // liquibaseProperties.s

        // primaryLiquibase.

        return args -> {
            System.out.println("My Database Product is : "+DatabaseFactory.getInstance().getDatabase(primaryLiquibase.getDatabaseProductName().toLowerCase()));
            if (employeeRepository.findAll().isEmpty()) {
                employeeRepository.saveAll(
                        getTestEmployees()
                );
            }
            if (employerRepository.findAll().isEmpty()) {
                employerRepository.saveAll(
                        getTestEmployers()
                );
            }
            System.out.println(employerRepository.findAll().size() + " no of records..");
        };
    }

    private List<Employer> getTestEmployers() {
        return Arrays.asList(
                new Employer(UUID.randomUUID().toString(), "Dhiren", 27),
                new Employer(UUID.randomUUID().toString(), "Dhiren", 27),
                new Employer(UUID.randomUUID().toString(), "Dhiren", 27),
                new Employer(UUID.randomUUID().toString(), "Dhiren", 27),
                new Employer(UUID.randomUUID().toString(), "Dhiren", 27),
                new Employer(UUID.randomUUID().toString(), "Dhiren", 27),
                new Employer(UUID.randomUUID().toString(), "Dhiren", 27),
                new Employer(UUID.randomUUID().toString(), "Dhiren", 27)
        );
    }

    private List<Employee> getTestEmployees() {
        return Arrays.asList(
                new Employee(UUID.randomUUID().toString(), "Dhiren", 27),
                new Employee(UUID.randomUUID().toString(), "Dhiren", 27),
                new Employee(UUID.randomUUID().toString(), "Dhiren", 27),
                new Employee(UUID.randomUUID().toString(), "Dhiren", 27),
                new Employee(UUID.randomUUID().toString(), "Dhiren", 27),
                new Employee(UUID.randomUUID().toString(), "Dhiren", 27),
                new Employee(UUID.randomUUID().toString(), "Dhiren", 27),
                new Employee(UUID.randomUUID().toString(), "Dhiren", 27)
        );
    }

    private Database getDataBase() throws DatabaseException {
        //primaryLiquibase.getDatabaseProductName()
        //DataTypeFactory

        return DatabaseFactory.getInstance().getDatabase(primaryLiquibase.getDatabaseProductName().toLowerCase());
    }

    private CreateTableStatement getCreateTableStatement(Database database) {
        CreateTableStatement statement = new CreateTableStatement(
                liquibaseProperties.getDefaultSchema(), liquibaseProperties.getLiquibaseSchema(),"TEST"
        );
        statement.addColumn("id", DataTypeFactory.getInstance().fromDescription("varchar2(10)", database));
        statement.addColumn("name", DataTypeFactory.getInstance().fromDescription("varchar2(20)", database));
        return statement;
    }

}
