package com.dhiren.springbootdockercompose.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
@AttributeOverrides({
        @AttributeOverride(
                name = "name", column = @Column(name = "guardianName")
        ),
        @AttributeOverride(
                name = "email", column = @Column(name = "guardianEmail")
        ),
        @AttributeOverride(
                name = "mobileNumber", column = @Column(name = "guardianMobile")
        )
})
@Builder
public class Guardian {

    private String name;
    private String email;
    private String mobileNumber;

}
