FROM openjdk:8u111-jdk-alpine

# Default Environment
ARG DB_HOST=127.0.0.1:3306
ARG DB_NAME=springboot
ARG DB_USER=dhiren
ARG DB_PASSWORD=password
ARG DEPENDENCY=target/dependency

# Dynamic Environment
ENV DB_HOST=$DB_HOST \
  DB_NAME=$DB_NAME \
  DB_USER=$DB_USER \
  DB_PASSWORD=$DB_PASSWORD

VOLUME /tmp
EXPOSE 9090

COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
                                        COPY ${DEPENDENCY}/META-INF /app/META-INF
                                        COPY ${DEPENDENCY}/BOOT-INF/classes /app

ADD target/spring-boot-docker-compose-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-cp","app:app/lib/*","com.dhiren.springbootdockercompose.SpringBootDockerComposeApplication"]
